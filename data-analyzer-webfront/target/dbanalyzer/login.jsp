<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<div class="container">
    <div class="row">
        <div class="main-div col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Start form -->
            <form id="formoid" method="post">
                <div class="form-group">
                    <label for="name">Email</label>
                    <input type="text" class="form-control" id="name" placeholder="Username">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                </div>

                <div class="form-inline">
                    <div class="form-check  form-group">
                        <button class="btn btn-info" type="button" name="showpassword" id="showpassword"
                                value="Show Password">Show password
                        </button>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-info" type="button" name="checkConnection" id="checkConnection"
                                value="Check Connection">Check Connection
                        </button>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <div class="form-group">
                        <div id="response-text"></div>
                    </div>
                </div>
        </form>
    </div>
</div>
</div>
<script type='text/javascript'>
    jQuery(document).ready(function () {
        // Show password Button
        $("#showpassword").on('click', function () {
            var pass = $("#password");
            var fieldtype = pass.attr('type');
            if (fieldtype == 'password') {
                pass.attr('type', 'text');
                $(this).text("Hide Password");
            } else {
                pass.attr('type', 'password');
                $(this).text("Show Password");
            }
        });

        $("#checkConnection").on('click', function () {
            $.ajax({
                type: "GET",
                url: "/checkConnection",
                success: function (result) {
                    var dataObject = jQuery.parseJSON(result);
                    if (dataObject == true) {
                        $("#checkConnection").css('background-color', 'green');
                    } else {
                        $("#checkConnection").css('background-color', 'red');
                    }
                },
                error: function (result) {
                    console.log(result);
                }
            });
        });
        $("#formoid").submit(function (e) {
            $.ajax({
                type: "POST",
                url: "/login",
                data: {
                    name: $('#name').val(),
                    password: $('#password').val()
                },
                success: function (data) {
                    var result = JSON.stringify(data);
                    $("#response-text").html(result);
                },
                error: function (result) {
                    $("#response-text").html("Something going wrong! Sorry!");
                }
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });


    })
</script>