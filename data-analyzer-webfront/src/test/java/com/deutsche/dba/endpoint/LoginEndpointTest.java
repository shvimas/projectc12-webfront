package com.deutsche.dba.endpoint;

import com.deutsche.dba.services.UserService;
import deutschebank.entity.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class LoginEndpointTest {

    @Mock
    UserService userServiceMock;

    private LoginEndpoint loginEndpoint;
    private User user;

    @Before
    public void setUp() {
        user = new User("admin", "admin");

        userServiceMock = mock(UserService.class);
        when(userServiceMock.findUserByUserIdAndPwd(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
        when(userServiceMock.findUserByUserIdAndPwd("admin", "admin")).thenReturn(user);

        loginEndpoint = new LoginEndpoint();
        loginEndpoint.setUserService(userServiceMock);
    }

    @Test
    public void doPost_successful() {
        String userName = "admin";
        String password = "admin";
        Assert.assertEquals(user, loginEndpoint.doPost(userName, password));
    }

    @Test
    public void doPost_unsuccessful() {
        String userName = "admin";
        String password = "wrong pwd";
        Assert.assertNull(loginEndpoint.doPost(userName, password));
    }
}