package com.deutsche.dba.endpoint;

import deutschebank.dbutils.DBConnector;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.sql.Connection;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DBConnector.class})
public class DBConnectionEndpointTest {

    @Mock
    DBConnector dbConnectorMock;

    @Mock
    Connection connnectionMock;

    private DBConnectionEndpoint endpoint;

    @Before
    public void setUp() {
        connnectionMock = mock(Connection.class);
        mockStatic(DBConnector.class);
        when(DBConnector.getConnection()).thenReturn(connnectionMock);

        endpoint = new DBConnectionEndpoint();
    }

    @Test
    public void checkConnection_failure() {
        when(dbConnectorMock.getConnection()).thenReturn(null);
        Assert.assertFalse(endpoint.checkConnection());
    }

    @Test
    public void checkConnection_success() {
        when(dbConnectorMock.getConnection()).thenReturn(connnectionMock);
        Assert.assertTrue(endpoint.checkConnection());
    }
}