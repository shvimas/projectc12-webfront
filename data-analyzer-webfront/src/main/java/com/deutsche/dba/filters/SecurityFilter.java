package com.deutsche.dba.filters;

import com.deutsche.dba.services.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebFilter("/*")
public class SecurityFilter implements Filter {
    private List<String> OPEN_PAGES = new ArrayList<>();
    {
        {
            OPEN_PAGES.add("/login.jsp");
            OPEN_PAGES.add("/rws/login");
            OPEN_PAGES.add("/rws/logout");
            OPEN_PAGES.add("/rws/checkConnection");
        }
    }

    private UserService userService = new UserService();

    @Override
    public void init(FilterConfig filterConfig) {

    }

    private boolean checkLogin(ServletContext servletContext) {
        Object name = servletContext.getAttribute("username");
        Object pass = servletContext.getAttribute("password");
        if (name != null && pass != null) {
            return userService.findUserByUserIdAndPwd((String) name, (String) pass) != null;
        }
        return false;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        ServletContext servletContext = request.getServletContext();
        String pathInfo = request.getRequestURI();

        if (OPEN_PAGES.contains(pathInfo) || (servletContext != null && checkLogin(servletContext))) {
            filterChain.doFilter(request, response); // Logged-in user found, so just continue request.
        } else {
            response.sendRedirect(request.getContextPath() + "/login.jsp"); // No logged-in user found, so redirect to login page.
        }
    }

    @Override
    public void destroy() {

    }
}
