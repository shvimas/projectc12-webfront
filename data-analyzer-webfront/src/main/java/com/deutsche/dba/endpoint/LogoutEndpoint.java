package com.deutsche.dba.endpoint;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

@Path("/logout")
public class LogoutEndpoint {

    @Context
    private ServletContext context;

    @GET
    public Response goGet() throws URISyntaxException {
        if (context != null) {
            context.removeAttribute("username");
            context.removeAttribute("password");
        }
        return Response.seeOther(new URI("/")).build();
    }
}
