package com.deutsche.dba.endpoint;

import deutschebank.dao.UserDao;
import deutschebank.entity.Extra2Entity;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/extra/end")
public class EndPositionsEndpoint {

    private UserDao userDao = new UserDao();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public List<Extra2Entity> doPost() {
        return userDao.getEndPositions();
    }
}
