package com.deutsche.dba.services;

import deutschebank.dao.NoteDao;
import deutschebank.entity.Note;

import java.util.List;

public class NoteService {
    private NoteDao dao = new NoteDao();

    public List<Note> findNotesByOffsetAndLimit(int limit, int offset) {
        List<Note> notes = dao.findNoteListByOffsetAndLimit(limit, offset);
        return notes;
    }
}
