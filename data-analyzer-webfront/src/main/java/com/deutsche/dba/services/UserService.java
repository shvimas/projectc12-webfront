package com.deutsche.dba.services;

import deutschebank.dao.UserDao;
import deutschebank.entity.User;

import java.util.logging.Logger;

public class UserService {
    private static final Logger LOGGER = java.util.logging.Logger.getLogger(UserService.class.getName());
    private UserDao userDao = new UserDao();

    public User findUserByUserIdAndPwd(String userId, String userPwd) {
        User theUser = userDao.loadFromDB(userId, userPwd);

        if (theUser != null)
            LOGGER.info("User " + userId + " has logged into system");

        return theUser;
    }
}
