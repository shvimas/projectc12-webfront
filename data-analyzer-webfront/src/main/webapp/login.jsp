<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://d3js.org/d3.v5.min.js"></script>

<div class="container" id="page1">
    <div class="row">
        <div class="main-div col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Start form -->
            <form id="formoid" method="post">
                <div class="form-group">
                    <label for="name">Email</label>
                    <input type="text" class="form-control" id="name" placeholder="Username">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                </div>

                <div class="form-inline">
                    <div class="form-check  form-group">
                        <button class="btn btn-info" type="button" name="showpassword" id="showpassword"
                                value="Show Password">Show password
                        </button>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-info" type="button" name="checkConnection" id="checkConnection"
                                value="Check Connection">Check Connection
                        </button>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <div class="form-group">
                        <div id="response-text"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-3">
            <table class="table table-sm tableAvg table-striped table-bordered text-center">
                <thead class="thead0">
                <tr class="row">
                    <th class="col-lg-12">AVG B/S</th>
                </tr>
                </thead>
                <tbody class="tbody0">
                <tr class="row mainTableRow">
                    <td class="col-lg-4 mainTableColumn"><b>Name</b></td>
                    <td class="col-lg-4 mainTableColumn"><b>BuyPrice</b></td>
                    <td class="col-lg-4 mainTableColumn"><b>SellPrice</b></td>
                </tr>
                </tbody>
            </table>
            <table class="table table-sm tableRPL table-striped table-bordered text-center">
                <thead class="theadRPL">
                <tr class="row">
                </tr>
                </thead>
                <tbody class="col-lg-12 tbodyRPL">
                <tr class="row mainTableRow">
                    <th class="col-lg-6 mainTableColumn">Name</th>
                    <th class="col-lg-6 mainTableColumn">Realised P/L</th>
                </tr>
                </tbody>
            </table>
            <table class="table table-sm tableEnd table-striped table-bordered text-center">
                <thead class="theadEnd">
                <tr class="row">
                    <th class="col-lg-12">EndPosition</th>
                </tr>
                </thead>
                <tbody class="col-lg-12 tbodyEnd">
                <tr class="row mainTableRow">
                    <th class="col-lg-4 mainTableColumn">Name</th>
                    <th class="col-lg-4 mainTableColumn">Instrument</th>
                    <th class="col-lg-4 mainTableColumn">Net</th>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-9">
            <table class="table table-sm table1 table-striped table-bordered text-center">
                <thead>
                <tr class="row text-center">
                    <th class="col-lg-6 mainTableColumn"><b>Deal</b></th>
                    <th class="col-lg-2 mainTableColumn"><b>Instrument</b></th>
                    <th class="col-lg-4 mainTableColumn"><b>Counterparty</b></th>
                </tr>
                </thead>
                <tbody class="tbody1 col-lg-12">
                <tr class="row">
                    <th class="col-lg-1 mainTableColumn">id</th>
                    <th class="col-lg-1 mainTableColumn">type</th>
                    <th class="col-lg-1 mainTableColumn">amount</th>
                    <th class="col-lg-1 mainTableColumn">quantity</th>
                    <th class="col-lg-2 mainTableColumn">time (yy-mm-dd)</th>
                    <th class="col-lg-2 mainTableColumn">name</th>
                    <th class="col-lg-1 mainTableColumn">name</th>
                    <th class="col-lg-1 mainTableColumn">status</th>
                    <th class="col-lg-2 mainTableColumn">reg_date</th>
                </tr>
                </tbody>
            </table>
            <div class="active-cyan-4 mb-4 float-left">
                <input class="form-control" id="myInput" type="text" placeholder="Search" aria-label="Search">
            </div>
            <ul class="pagination float-right">
                <li class="page-item"><a class="page-link" onclick="decPage()" href="#">Previous</a></li>
                <li class="page-item" active><a class="active page-link cPage">0</a></li>
                <li class="page-item"><a class="page-link" onclick="incPage()" href="#">Next</a></li>
            </ul>
        </div>
    </div>
</div>

<script type='text/javascript'>
    function incPage() {
        $(".tbody1 .mainTableRow").remove();
        getMainJson(1).then(function (json1) {
            console.log(json1);
            tabulate(json1);
            $('.cPage').text(pageNumber)
        });
    }

    function decPage() {
        $(".tbody1 .mainTableRow").remove();
        getMainJson(-1).then(function (json2) {
            console.log(json2);
            tabulate(json2);
            $('.cPage').text(pageNumber)
        });
    }


    $("#myInput").keyup(function () {
        var value = this.value.toLowerCase().trim();

        $(".tbody1 tr").each(function (index) {
            if (!index) return;
            $(this).find("td").each(function () {
                var id = $(this).text().toLowerCase().trim();
                var not_found = (id.indexOf(value) == -1);
                $(this).closest('tr').toggle(!not_found);
                return not_found;
            });
        });
    });

    jQuery(document).ready(function () {
        // Show password Button
        $("#showpassword").on('click', function () {
            var pass = $("#password");
            var fieldtype = pass.attr('type');
            if (fieldtype == 'password') {
                pass.attr('type', 'text');
                $(this).text("Hide Password");
            } else {
                pass.attr('type', 'password');
                $(this).text("Show Password");
            }
        });

        $("#checkConnection").on('click', function () {
            $.ajax({
                type: "POST",
                url: "/rws/checkConnection",
                success: function (result) {
                    var dataObject = jQuery.parseJSON(result);
                    if (dataObject == true) {
                        $("#checkConnection").css('background-color', 'green');
                    } else {
                        $("#checkConnection").css('background-color', 'red');
                    }
                },
                error: function (result) {
                    console.log(result);
                }
            });
        });

        $("#formoid").submit(function (e) {
            $.ajax({
                type: "POST",
                url: "/rws/login",
                data: {
                    name: $('#name').val(),
                    password: $('#password').val()
                },
                success: function (data) {

                    username = $('#name').val();
                    pwd = $('#password').val();
                    console.log(username)
                    console.log(pwd)
                    var result = JSON.stringify(data);
                    $("#response-text").html(result);

                    getMainJson(0).then(function (json) {
                        console.log(json);

                        tabulate(json);
                    });
                },
                error: function (data) {
                    $("#response-text").html("Something going wrong! Sorry!");
                }
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

    });

    jQuery(document).ready(function () {
        getAvgJson().then(function (json) {
            console.log(json);

            tabulateAverage(json);
        });
    });

    jQuery(document).ready(function () {
        getEndPosJson().then(function (json) {
            console.log(json);
            var result = JSON.stringify(json);
            tabulateEndPos(json);
        });
    });

    jQuery(document).ready(function () {
        getRPLJson().then(function (json) {
            console.log(json);
            var result = JSON.stringify(json);
            $("#response-text").html(result);

            tabulateRPLPos(json);
        });
    });

    function tabulateAverage(json) {
        var tbody = d3.select('.tbody0');
        var rows = tbody.selectAll('tr')
            .data(json)
            .enter()
            .append('tr').attr('class', 'row mainTableRow');

        var cells = rows.selectAll('td')
            .data(function (row) {
                return columnsDataAverage.map(function (column) {
                    return {column: column, value: row[column]};
                });
            })
            .enter()
            .append('td').attr('class', 'col-lg-4 mainTableColumn')
            .text(function (d) {
                return d.value;
            });
    }

    function tabulateEndPos(json) {
        var tbody = d3.select('.tbodyEnd');
        var rows = tbody.selectAll('tr')
            .data(json)
            .enter()
            .append('tr').attr('class', 'row mainTableRow');

        var cells = rows.selectAll('td')
            .data(function (row) {
                return columnsDataEndPoint.map(function (column) {
                    return {column: column, value: row[column]};
                });
            })
            .enter()
            .append('td').attr('class', 'col-lg-4 mainTableColumn')
            .text(function (d) {
                return d.value;
            });
    }

    function tabulateRPLPos(json) {
        var tbody = d3.select('.tbodyRPL');
        var rows = tbody.selectAll('tr')
            .data(json)
            .enter()
            .append('tr').attr('class', 'row mainTableRow');

        var cells = rows.selectAll('td')
            .data(function (row) {
                return columnsDataRPLPoint.map(function (column) {
                    return {column: column, value: row[column]};
                });
            })
            .enter()
            .append('td').attr('class', 'col-lg-4 mainTableColumn')
            .text(function (d) {
                return d.value;
            });
    }

    function tabulate(json) {
        var tbody = d3.select('.tbody1');
        var rows = tbody.selectAll('tr')
            .data(json)
            .enter()
            .append('tr').attr('class', 'row mainTableRow');

        var cells = rows.selectAll('td')
            .data(function (row) {
                return columnsData.map(function (column) {
                    return {column: column, value: row[column]};
                });
            })
            .enter()
            .append('td').attr('class', 'col-lg-1 mainTableColumn')
            .text(function (d) {
                return d.value;
            });

        rows.selectAll('td:nth-child(5)').attr('class', 'col-lg-2 mainTableColumn');
        rows.selectAll('td:nth-child(6)').attr('class', 'col-lg-2 mainTableColumn');
        rows.selectAll('td:nth-child(9)').attr('class', 'col-lg-2 mainTableColumn');
    }

    var pageSize = 20;
    var pageNumber = 0;
    var username = "";
    var pwd = "";


    function getMainJson(value) {
        pageNumber += value;
        console.log(pageNumber)
        if (pageNumber < 0)
            pageNumber = 0;

        return d3.json('/rws/notes/' + pageSize * pageNumber + '/' + pageSize, {
            method: "GET",
            headers: {
                "Content-type": " application/x-www-form-urlencoded"
            }
        });
    }

    function getAvgJson() {
        return d3.json('/rws/extra/avgprices', {
            method: "POST",
            headers: {
                "Content-type": " application/x-www-form-urlencoded"
            }
        });
    }

    function getEndPosJson() {
        return d3.json('/rws/extra/end', {
            method: "POST",
            headers: {
                "Content-type": " application/x-www-form-urlencoded"
            }
        });
    }

    function getRPLJson() {
        return d3.json('/rws/extra/rpl', {
            method: "POST",
            headers: {
                "Content-type": " application/x-www-form-urlencoded"
            }
        });
    }

    var columnsData = ['dealId', 'dealType', 'dealQuantity', 'dealAmount', 'dealTime', 'instrumentName', 'counterpartyName', 'counterpartyStatus', 'counterpartyDateRegistered'];
    var columnsDataAverage = ['instrumentName', 'averageSellPrice', 'averageBuyPrice'];
    var columnsDataEndPoint = ['counterparty', 'instrument', 'net'];
    var columnsDataRPLPoint = ['dealerName', 'profitLoss'];

</script>